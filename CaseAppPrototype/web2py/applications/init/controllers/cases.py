# -*- coding: utf-8 -*-


def index():
    # Get breadcrumbs
    breadcrumbs = get_breadcrumbs(sep='<i class="fa fa-fw fa-angle-right"></i>')

    # Create cases grids
    active_grid = LOAD(c='cases', f='grid.load', args=['active'], ajax=False)
    closed_grid = LOAD(c='cases', f='grid.load', args=['closed'], ajax=False)

    return dict(breadcrumbs=breadcrumbs,
                active_grid=active_grid,
                closed_grid=closed_grid)


def grid():
    status = request.args(0) or 'active'
    if status == 'active':
        cases = db.case_file.active_cases()
    else:
        cases = db.case_file.closed_cases()

    return dict(status=status, cases=cases)


def view():

    # Get breadcrumbs
    breadcrumbs = get_breadcrumbs(sep='<i class="fa fa-fw fa-angle-right"></i>')

    # Create virtual fields specifically for this view
    db.case_file.view_status = Field.Virtual('status', lambda row: None, label='Status')
    db.case_file.current_status = Field.Virtual('current_status', lambda row: None, label='Current Status')
    db.case_file.next_steps = Field.Virtual('next_steps', lambda row: None, label='Next Steps')

    case_number = request.args(0) or redirect(URL('cases', 'index'))
    is_director = auth.has_membership(user_id=auth.user_id, role='Directors')

    query = (db.case_file.case_number == str(case_number))
    case = db(query).select().render(0)

    most_recent_note = db.status_note.most_recent_note(case.id)
    case.view_status = most_recent_note.phase
    case.current_status = most_recent_note
    case.next_steps = most_recent_note.next_steps

    notes = db.case_file.status_notes(case.id)

    create_form = LOAD('notes', 'create.load', args=[case.id],
                       ajax=True, ajax_trap=True,
                       target="create-form-container")

    update_form = DIV(_id="update-form-container", ajax=True)

    return dict(breadcrumbs=breadcrumbs, case=case, notes=notes, create_form=create_form,
                update_form=update_form, is_director=is_director)
