# -*- coding: utf-8 -*-


def check_director_membership():
    if not auth.has_membership(user_id=auth.user_id, role='Directors'):
        redirect(URL('default', 'index'))
    return True

def index():

    # Get breadcrumbs
    breadcrumbs = get_breadcrumbs(sep='<i class="fa fa-fw fa-angle-right"></i>')

    return dict(breadcrumbs=breadcrumbs)

def current_status():


    # Get breadcrumbs
    breadcrumbs = get_breadcrumbs(sep='<i class="fa fa-fw fa-angle-right"></i>')

    if request.extension == 'html':
        selected = request.vars.get('selected') or {}
    else:
        try:
            from gluon.contrib import simplejson as json
            selected = json.loads(request.vars.get('selected'))
        except:
            selected = {}

    filters_context = filter_context(selected)

    # Get filters html
    filters = XML(response.render('reports/current_status_filters.load',
                                  filters_context))

    # Get report context
    report_context = rpt_context(filters_context['filters'], selected)

    if request.extension != 'html':
        # render pdf if request.extension == 'pdf'
        if request.extension == 'pdf':
            return render_pdf(report_context)
        return XML(response.render('reports/current_status_html.load',
                                   report_context))

    report = XML(response.render('reports/current_status_html.load',
                                 report_context))


    return dict(breadcrumbs=breadcrumbs, filters=filters, report=report)
