def create():
    # Hide system fields
    for f in ['legacy_id', 'case_id', 'created_on', 'is_active',
              'created_by', 'updated_on', 'updated_by']:
        db.status_note[f].readable = db.status_note[f].writable = False

    # get case id
    case_id = request.args(-1) or redirect('cases', 'index')

    most_recent_note = db.status_note.most_recent_note(case_id) or None

    # set field value defaults
    db.status_note.case_id.default = case_id
    db.status_note.reported_by.default = auth.user_id
    db.status_note.phase.default = most_recent_note.phase if most_recent_note else None

    # create SQLFORM
    form = SQLFORM(db.status_note, record=None, readonly=False,
                   formstyle=bs3.form(layout='horizontal'),
                   _class='form-horizontal')

    # handle form processing
    if form.process().accepted:
        response.flash = None
        response.js = """
            jQuery(document).ready(function($) {
                var reportSpinner = new Spinner().spin();
                var pdfSpinner = new Spinner().spin();
                $modal = $('.modal-body').empty();
                $($modal).css({'text-align': 'center',
                           'padding': '120px',
                           });
                $($modal).append(reportSpinner.el);
            });
        """
        case = db(db.case_file.id==case_id).select().first()
        redirect(URL('cases', 'view', extension=False, args=[case.case_number]), client_side=True)
    elif form.errors:
        response.flash = 'Please correct the errors in your form and resubmit.'

    return dict(form=form)


def update():

    # Handle delete requests
    if request.args and request.args(0) == 'delete':
        response.flash = None
        response.js = """
            jQuery(document).ready(function($) {
                var reportSpinner = new Spinner().spin();
                var pdfSpinner = new Spinner().spin();
                $modal = $('.modal-body').empty();
                $($modal).css({'text-align': 'center',
                           'padding': '120px',
                           });
                $($modal).append(reportSpinner.el);
            });
        """
        note = db(db.status_note.id==request.args(-1)).select().first()
        note.update_record(is_active=False)
        case = db(db.case_file.id==note.case_id).select().first()
        redirect(URL('cases', 'view', args=[case.case_number]), client_side=True)

    # Hide system fields
    for f in ['legacy_id', 'case_id', 'created_on', 'is_active',
              'created_by', 'updated_on', 'updated_by']:
        db.status_note[f].readable = db.status_note[f].writable = False

    # Get status note record
    record = request.args(-1) or redirect(request.env.http_referer)
    note = db(db.status_note.id==record).select().first()

    # Create SQLFORM
    form = SQLFORM(db.status_note, record=record, readonly=False,
                   formstyle=bs3.form(layout='horizontal'),
                   _class='form-horizontal')

    # Handle SQLFORM processing
    if form.process().accepted:
        response.flash = None
        response.js = """jQuery(document).ready(function($) {
            $modal = $('.modal-content').empty();
            $($modal).css({'text-align': 'center',
                           'padding': '130px',
                           });
            $($modal).html('<i class="fa fa-refresh fa-spin fa-5x"></i>');});
            """
        case = db(db.case_file.id==note.case_id).select().first()
        redirect(URL('cases', 'view', extension=False, args=[case.case_number]), client_side=True)
    elif form.errors:
        response.flash = form.errors

    return dict(note=note, form=form)



def update_test():

    # Hide system fields
    for f in ['legacy_id', 'case_id', 'created_on', 'is_active',
              'created_by', 'updated_on', 'updated_by']:
        db.status_note[f].readable = db.status_note[f].writable = False

    # set variables defaults
    record = None
    readonly = False
    user_can_edit = auth.has_membership(user_id=auth.user_id, role='Directors')

    if request.args:
        action = request.args(0)
        if action == 'delete':
            db(db.status_note.id==record).update_record(is_active=False)
            redirect(URL('cases', 'index'))

        elif action == 'new':
            case_id = request.args(-1)
            db.status_note.case_id.default = case_id
            db.status_note.reported_by.default = auth.user_id
            db.status_note.phase.default = \
                db(db.status_note.case_id==case_id).select(db.status_note.phase,
                                                           orderby=~db.status_note.status_date).first().phase

        else:
            record = request.args(-1)
            row = db(db.status_note.id == record).select().first()
            case_id = row.case_id
            if not user_can_edit and not auth.user_id == row.reported_by:
                readonly = True

    if not user_can_edit:
        db.status_note.reported_by.writable = False
        db.status_note.status_date.writable = False

    form = SQLFORM(db.status_note, record=record, readonly=readonly,
                   formstyle=bs3.form(layout='horizontal'),
                   _class='form-horizontal')

    if form.process().accepted:
        response.flash = None
        response.js = """jQuery(document).ready(function($) {
            $modal = $('.modal-content').empty();
            $($modal).css({'text-align': 'center',
                           'padding': '130px',
                           });
            $($modal).html('<i class="fa fa-refresh fa-spin fa-5x"></i>');});
            """
        case = db(db.case_file.id==case_id).select().first()
        redirect(URL('cases', 'view', extension=False, args=[case.case_number]), client_side=True)
    elif form.errors:
        response.flash = 'Please correct the errors in your form and resubmit.'
    else:
        print form
    return dict(action=action, user_can_edit=user_can_edit,
                record=record, form=form)
