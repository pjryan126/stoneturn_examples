#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os


def run():
    db.executesql('IF EXISTS DROP INDEX CaseNumber_Index')
    db.commit()
    db.executesql('CREATE INDEX CaseNumber_Index ON [App].[Case] (CaseNumber)')
    db.commit()
    scripts = ['migrate_users', 'migrate_cases', 
               'migrate_hours', 'migrate_cases', 'migrate_status_notes']
    for s in scripts:
        fpath = os.path.join(request.folder, 'private', 'sql', '{0}.sql'.format(s))
        f = open(os.path.abspath(fpath))
        sql = ' '.join(f.readlines())
        db.executesql(sql)
        db.commit()
        f.close()
    return

if __name__ == '__main__':
    run()



