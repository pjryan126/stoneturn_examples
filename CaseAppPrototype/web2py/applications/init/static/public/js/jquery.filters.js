(function($) {
    $.extend({
        filters: function(reportUrl, pdfUrl) {
            function isChecked(checkboxId) {
                var id = '#' + checkboxId;
                return $(id).is(":checked");
            }
            // Reset filters
            function resetFilters() {
                $("#report-filters input").prop('checked', true);
            }
            resetFilters();
            $("#reset-filters").on('click', function() {
                resetFilters();
                var filters = getFilters();
                applyFilters(filters, reportUrl, '.report-wrapper');
                var url = pdfUrl + '?selected=' + encodeURIComponent(JSON.stringify(filters));
                $("#print-pdf").attr("href", url);
            });
            $('.select-all').on('click', function() {
                var id = this.id;
                $(this).closest('.checkbox').siblings().find('input').each(function() {
                    $(this).prop('checked', isChecked(id));
                });
            });
            // Get filters
            var getFilters = function() {
                var filters = {};
                $(".checklist-form").each(function() {
                    var group = this.id.replace('-', '_');
                    filters[group] = [];
                    $(this).find("input.item:checked").each(function() {
                        var id = $(this).attr('id');
                        if (!(isNaN(id))) {id=parseInt(id)}
                        else if (id=="True") {id=true}
                        else if (id=="False") {id=false};
                        filters[group].push(id);
                    });
                });
                return filters;
            }
            // Apply filters
            function applyFilters(filters, dataUrl, target) {
                var opts = {top: '150px'};
                var spinner = new Spinner(opts).spin();
                $(target).empty().append(spinner.el);
                $.ajax({
                    type: 'GET',
                    url: dataUrl,
                    data: {'selected': JSON.stringify(filters)},
                    contentType: "application/json;charset=UTF-8",
                    success: function (data, textStatus, jqXHR) {
                        spinner.stop();
                        $(target).html(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            }
            $("#report-filters").on('change', function() {
                var filters = getFilters();
                applyFilters(filters, reportUrl, '.report-wrapper');
                var url = pdfUrl + '?selected=' + encodeURIComponent(JSON.stringify(filters));
                $("#print-pdf").attr("href", url);
            });
        }
    });
})(jQuery);