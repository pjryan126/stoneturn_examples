#!/usr/bin/env python
# -*- coding: utf-8 -*-

from applications.casetrak.modules.utils import format_money, pluralize
from gluon import *
import string


def clean(s):
    try:
        s = filter(lambda x: x in string.printable, s)
    except:
        pass
    return s


def filter_notes(tablename, rows, selected={}):
    if len(selected.keys()) > 0:
        for key, values in selected.iteritems():
            rows = rows.find(lambda row:
                               row[tablename][key] in values)
    return rows


def org_case(notes, is_active):
    from gluon.storage import Storage

    db = current.globalenv['db']

    case_fields = ['name', 'case_number', 'segment',
                   'region', 'country', 'description',
                   'key_subject', 'exposure_amount',
                   'status', 'days_open', 'total_hrs',
                   'two_wk_hrs', 'director', 'sr_mgr',
                   'lead', 'resp_exec', 'last_activity_on',
                   'is_privileged', 'conclusion', 'closed_on']

    organized_notes = []
    print notes
    for i, note in enumerate(notes):
        print note
        if note.case_file.is_active == is_active:

            rnote = notes.render(i)
            for f in rnote.case_file:
                rnote.case_file[f] = clean(rnote.case_file[f])
            for f in rnote.status_note:
                rnote.status_note[f] = clean(rnote.status_note[f])

            if not any(o.get('case_number', None) == note.case_file.case_number for o in organized_notes):

                o = Storage()
                for c in case_fields:
                    rnote.case[c] = rnote.case[c]
                    o[c] = rnote.case[c]
                o.notes = []
                organized_notes.append(o)
            for o in organized_notes:
                if o.case_number == note.case_file.case_number:
                    o.notes.append(rnote)
                    continue

    return organized_notes


def filters_context(selected):
    from gluon.tools import Storage

    db = current.globalenv['db']
    cases = db.case_file.all_cases()
    fields = ['is_privileged', 'segment', 'region', 'country',
              'director', 'sr_mgr', 'lead']

    filters = [Storage(dict(name=f,
                            title=pluralize(db.case_file[f].label),
                            available=[[value,
                                        value if isinstance(value, bool) else
                                        db.case_file[f].represent(value)]
                                       for value in sorted(set([v[f] for v in cases]))],
                            selected=[s for s in selected.get(f)]
                            if selected.get(f) is not None
                            else [value for value in sorted(set([v[f] for v in cases]))]
                            ))
               for f in fields]

    return dict(filters=filters)


def report_context(filters, selected):
    db = current.globalenv['db']
    notes = db.case_file.report_notes()
    if len(selected.keys()) > 0:
        notes = filter_notes('case_file', notes, selected)

    active_cases = org_case(notes, is_active=True)
    closed_cases = org_case(notes, is_active=False)

    return dict(filters=filters,
                active_cases=active_cases,
                closed_cases=closed_cases)


def render_pdf(context):
    import cStringIO
    import os
    from xhtml2pdf import pisa

    request = current.request
    response = current.response

    context['logo'] = os.path.abspath(
        os.path.join(request.folder, 'static', 'src/img/favicon.jpg'))
    context['checkbox_empty'] = os.path.abspath(
        os.path.join(request.folder, 'static', 'src/img/checkbox_empty.png'))
    context['checkbox_checked'] = os.path.abspath(
        os.path.join(request.folder, 'static', 'src/img/checkbox_checked.png'))
    src = response.render('reports/current_status_pdf.html', context)
    res = cStringIO.StringIO()

    pisa.CreatePDF(src, dest=res)
    pdf = res.getvalue()
    res.close()

    return pdf
