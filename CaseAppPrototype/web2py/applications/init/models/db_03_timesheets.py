# -*- coding: utf-8 -*-
from datetime import datetime, timedelta

today = datetime.today()
two_weeks_ago = today - timedelta(days=14)

db.define_table('tsheet',
                Field('user_id',
                      'reference auth_user'),
                Field('case_id',
                      'reference case_file'),
                Field('entry_date',
                      'datetime'),
                Field('category_number',
                      'string',
                      length=4),
                Field('hours',
                      'decimal(10, 2)'),
                signature,
                migrate=settings.migrate)

@db.tsheet.add_method.total_hrs
def total_hrs(self, case_id):
    query = (self.case_id == case_id)
    sum = self.hours.sum()
    return db(query).select(sum).first()[sum]

@db.tsheet.add_method.two_wk_hrs
def two_wk_hrs(self, case_id):
    query = (self.case_id == case_id)
    query &= (self.entry_date >= two_weeks_ago)
    sum = self.hours.sum()
    return db(query).select(sum).first()[sum]

