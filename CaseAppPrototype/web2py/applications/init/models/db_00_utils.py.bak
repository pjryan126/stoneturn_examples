#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gluon import current
from gluon.html import (
    A,
    URL,
    XML,
)

def format_money(value, places=0, curr='', sep=',', dp='.', pos='', neg='(', trailneg=''):
    """Convert Decimal to a money formatted string.

    places:  required number of places after the decimal point
    curr:    optional currency symbol before the sign (may be blank)
    sep:     optional grouping separator (comma, period, space, or blank)
    dp:      decimal point indicator (comma or period)
             only specify as blank when places is zero
    pos:     optional sign for positive numbers: '+', space or blank
    neg:     optional sign for negative numbers: '-', '(', space or blank
    trailneg:optional trailing minus indicator:  '-', ')', space or blank



    """
    from decimal import Decimal
    q = Decimal(10) ** -places      # 2 places --> '0.01'
    sign, digits, exp = value.quantize(q).as_tuple()
    result = []
    digits = map(str, digits)
    build, next = result.append, digits.pop

    if places == 0:
        dp = ''

    if sign:
        build(trailneg)
    for i in range(places):
        build(next() if digits else '0')

    build(dp)
    if not digits:
        build('0')

    i = 0
    while digits:
        build(next())
        i += 1
        if i == 3 and digits:
            i = 0
            build(sep)
    build(curr)
    build(neg if sign else pos)
    return ''.join(reversed(result))


def breadcrumbs(root=None, sep=' > '):
    """Create breadcrumb links for current request"""
    request = current.request
    T = current.T

    # make links pretty by capitalizing and using 'home' instead of 'default'
    pretty = lambda s: s.replace('default', 'Home').replace('_', ' ').title()

    menus = [A(T('Home'), _href=URL(r=request, c='default', f='index'))]

    if request.controller != 'default':
        # add link to current controller
        menus.append(A(T(pretty(request.controller)), _href=URL(r=request, c=request.controller, f='index')))
        if request.function == 'index':
            # are at root of controller
            menus[-1] = A(T(pretty(request.controller)), _href=URL(r=request, c=request.controller, f=request.function))
        else:
            # are at function within controller
            menus.append(A(T(pretty(request.function)), _href=URL(r=request, c=request.controller, f=request.function)))
        # you can set a title putting using breadcrumbs('My Detail Title')
        if request.args and root:
            menus.append(A(T(root)), _href=URL(r=request, c=request.controller, f=request.function,args=[request.args]))
    else:
        # menus.append(A(pretty(request.controller), _href=URL(r=request, c=request.controller, f='index')))
        if request.function == 'index':
            # are at root of controller
            #menus[-1] = pretty(request.controller)
            pass
            #menus.append(A(pretty(request.controller), _href=URL(r=request, c=request.controller, f=request.function)))
        else:
            # are at function within controller
            menus.append(A(T(pretty(request.function)), _href=URL(r=request, c=request.controller, f=request.function)))
        # you can set a title putting using breadcrumbs('My Detail Title')
        if request.args and root:
            menus.append(A(T(root), _href=URL(r=request, f=request.function,args=[request.args])))

    return XML(sep.join(str(m) for m in menus))


def pluralize(singular):
    """Return plural form of given lowercase singular word (English only). Based on
    ActiveState recipe http://code.activestate.com/recipes/413172/

    """

    ABERRANT_PLURAL_MAP = {
    'appendix': 'appendices',
    'child': 'children',
    'criterion': 'criteria',
    'focus': 'foci',
    'index': 'indices',
    'leaf': 'leaves',
    'life': 'lives',
    'man': 'men',
    'nucleus': 'nuclei',
    'person': 'people',
    'phenomenon': 'phenomena',
    'Privileged': 'Privileged',
    'self': 'selves',
    'syllabus': 'syllabi',
    'training': 'training',
    'woman': 'women',
    }

    VOWELS = set('aeiou')

    if not singular:
        return ''
    plural = ABERRANT_PLURAL_MAP.get(singular)
    if plural:
        return plural
    root = singular
    try:
        if singular[-1] == 'y' and singular[-2] not in VOWELS:
            root = singular[:-1]
            suffix = 'ies'
        elif singular[-1] == 's':
            if singular[-2] in VOWELS:
                if singular[-3:] == 'ius':
                    root = singular[:-2]
                    suffix = 'i'
                else:
                    root = singular[:-1]
                    suffix = 'ses'
            else:
                suffix = 'es'
        elif singular[-2:] in ('ch', 'sh'):
            suffix = 'es'
        else:
            suffix = 's'
    except IndexError:
        suffix = 's'
    plural = root + suffix
    return plural
