# -*- coding: utf-8 -*-

from gluon.storage import Storage
settings = Storage()

settings.phase = 'Testing'


settings.db_uri = 'sqlite://dev.db'
settings.init_db = True
settings.migrate = True
settings.track_changes = True

settings.title = 'CaseTrak'
settings.subtitle = 'Under Construction'
settings.author = 'Patrick J. Ryan'
settings.author_email = 'patrick.ryan@aig.com'
settings.keywords = ''
settings.description = 'CaseTrack Status Updates'
settings.layout_theme = 'Default'
settings.security_key = ''
settings.email_server = 'localhost:25'
settings.email_sender = 'patrick.ryan@aig.com'
settings.email_login = None
settings.login_method = 'local'
settings.login_config = ''
settings.theme = 'inverse'

response.generic_patterns = ['*']
from gluon.custom_import import track_changes
track_changes(settings.track_changes)
