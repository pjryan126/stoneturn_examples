# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## Customize your APP title, subtitle and menus here
#########################################################################

response.logo = A(IMG(_src=URL(c='static', f='src/img/favicon.jpg'),
                      _alt='AIG'),
                  _class='navbar-brand')
response.title = settings.title
response.subtitle = settings.subtitle
response.layout_theme = 'blue'

#########################################################################
## this is the main application menu add/remove items as required
#########################################################################

response.menu = []

if "auth" in locals(): auth.wikimenu()

