# -*- coding: utf-8 -*-
from datetime import datetime, timedelta

today = datetime.today()

PHASES = ['Planning', 'Fieldwork', 'Reporting', 'Follow-Up']

db.define_table('segment',
                Field('name',
                      'string',
                      length=128),
                signature,
                format='%(name)s',
                migrate=settings.migrate)

db.define_table('region',
                Field('name',
                      'string',
                      length=128),
                signature,
                format='%(name)s',
                migrate=settings.migrate)

db.define_table('country',
                Field('iso',
                      'string',
                      length=2),
                Field('name',
                      'string',
                      length=128),
                Field('nice_name',
                      'string',
                      length=128),
                Field('iso3',
                      'string',
                      length=3,),
                Field('number_code',
                      'integer'),
                Field('phone_code',
                      'integer'),
                signature,
                format='%(nice_name)s',
                migrate=settings.migrate)

db.define_table('state_province',
                Field('country_id',
                      'reference country'),
                Field('name',
                      'string',
                      length=128),
                Field('abbreviation',
                      'string',
                      length=128),
                signature,
                migrate=settings.migrate)

db.define_table('fraud_type',
                Field('name',
                      'string',
                      length=100),
                signature,
                format='%(name)s',
                migrate=True)

db.define_table('case_file',
                Field('legacy_id',
                      'integer'),
                Field('case_number',
                      'string',
                      unique=True),
                Field('name',
                      'string',
                      length=100,
                      label='Case'),
                Field('description',
                      'text',
                      label='Summary'),
                Field('conclusion',
                      'text'),
                Field('segment',
                      'reference segment',
                      label='Business Segment'),
                Field('region',
                      'reference region',
                      label='Region'),
                Field('country',
                      'reference country',
                      label='Country'),
                Field('state_id',
                      'reference state_province'),
                Field('key_subject',
                      'string', length=100),
                Field('director',
                      db.auth_user),
                Field('sr_mgr',
                      db.auth_user,
                      label='Senior Manager'),
                Field('lead',
                      db.auth_user,
                      label='Case Lead'),
                Field('resp_exec',
                      'string', length=100,
                      label='Responsible Executive'),
                Field('is_privileged',
                      'boolean',
                      label='Privileged',
                      default=False),
                Field('exposure_amount',
                      'decimal(10,2)',
                      label='Exposure Amount'),
                Field('confirmed_loss',
                      'decimal(10,2)'),
                Field('pending_recovery',
                      'decimal(10,2)'),
                Field('last_activity_on',
                      'datetime',
                      label='Date of Last Activity'),
                Field('is_control_issue',
                      'boolean',
                      default=False),
                Field('is_fraud',
                      'boolean',
                      default=False),
                Field('total_hrs',
                      'decimal(10,2)',
                      label='Total Hours'),
                Field('opened_on',
                      'datetime',
                      default=request.now),
                Field('opened_by',
                      db.auth_user,
                      default=auth.user_id),
                Field('closed_on',
                      'datetime'),
                Field('closed_by',
                      db.auth_user),
                signature,
                migrate=settings.migrate)

from uuid import uuid4
db.case_file.case_number.default = lambda: str(uuid4())[:7]

db.case_file.last_activity_on.represent = lambda d: d.strftime('%b %d %Y') if d else 'Not Available'
db.case_file.closed_on.represent = lambda d: d.strftime('%b %d %Y') if d else 'Not Available'

db.case_file.two_wk_hrs = \
    Field.Virtual('two_wk_hrs',
                  lambda row:
                  db.tsheet.two_wk_hrs(row.case_file.id),
                  label='Hours Last Two Weeks')

db.case_file.days_open = \
    Field.Virtual('days_open',
                  lambda row:
                  (today - row.case_file.opened_on).days)

db.case_file.status = \
        Field.Virtual('status',
                      lambda row: db.status_note.most_recent_note(row.case_file.id).phase,
                      label='Status')


# Format money fields
db.case_file.exposure_amount.represent = \
    lambda id, row: format_money(row.exposure_amount, places=2, curr='$')
db.case_file.confirmed_loss.represent = \
    lambda id, row: format_money(row.confirmed_loss, places=2, curr='$')
db.case_file.pending_recovery.represent = \
    lambda id, row: format_money(row.pending_recovery, places=2, curr='$')


# add methods to db.case_file table for convenience
@db.case_file.add_method.all_cases
def all_cases(self):
    query = (self.is_active==True)
    query |= ((self.is_active==False) & \
             (self.closed_on >= (datetime.today() - timedelta(days=14))))
    cases = db(query).select(orderby=~db.case_file.case_number)
    return cases


@db.case_file.add_method.active_cases
def active_cases(self):
    query = (self.is_active == True)
    cases = db(query).select(orderby=~db.case_file.case_number)
    return cases


@db.case_file.add_method.closed_cases
def closed_cases(self):
    query = (self.is_active == False) & \
            (self.closed_on >= (datetime.today() - timedelta(days=14)))
    cases = db(query).select(orderby=~db.case_file.case_number)
    return cases


@db.case_file.add_method.status_notes
def status_notes(self, case_id):
    query = (db.status_note.case_id==case_id)
    query &= (db.status_note.is_active==True)
    notes = db(query).select(db.status_note.ALL,
                             left=db.status_note.on(db.case_file.id==db.status_note.case_id),
                             orderby=~db.status_note.status_date)
    return notes


@db.case_file.add_method.report_notes
def report_notes(self):

    query = (self.is_active==True)
    query |= (self.closed_on >= (datetime.today() - timedelta(days=14)))
    notes = db(query).select(db.case_file.ALL, db.status_note.ALL,
                             left=db.status_note.on(db.case_file.id==db.status_note.case_id),
                             orderby=(~db.case_file.case_number, ~db.status_note.status_date))
    return notes


@db.case_file.add_method.filter_cases
def filter_cases(self, cases, selected={}):
    if selected is not None:
        for key, values in selected.iteritems():
            cases = cases.find(lambda row:
                               row[key] in values)
    return cases


@db.case_file.add_method.get_filters
def get_filters(self, cases, selected={}):
    # Set filter fields
    fields = ['is_privileged', 'segment', 'region', 'country',
              'director', 'sr_mgr', 'lead']
    for f in fields:
        selected.get(f)
    filters = [Storage(dict(name=f,
                            title=pluralize(db.case_file[f].label),
                            available=[[value,
                                        value if isinstance(value, bool) else
                                        db.case_file[f].represent(value)]
                                       for value in sorted(set([v[f] for v in cases]))],
                            selected=[s for s in selected.get(f)]
                            if selected.get(f) is not None
                            else [value for value in sorted(set([v[f] for v in cases]))]
                            ))
               for f in fields]

    return filters
