# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

from gluon import current

db = DAL(settings.db_uri, db_codec='latin1')

# MSSQL has problems with circular references in tables
# that have ONDELETE CASCADE. This is a MSSQL bug
for key in ['reference', 'reference FK']:
    db._adapter.types[key] = db._adapter.types[key].replace(
        '%(on_delete_action)s', 'NO ACTION')

session.connect(request, response, db)

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []

from gluon.tools import *

auth = Auth(db)  # authentication/authorization
service = Service()  # for json, xml, jsonrpc, xmlrpc, amfrpc
plugins = PluginManager()  # for plugins

auth.settings.extra_fields['auth_user'] = [
    Field('department', 'string', length=4),
    Field('title', 'string', length=40),
    Field('created_on', 'datetime',
          default=request.now),
    Field('created_by', 'reference auth_user',
          default=auth.user_id),
    Field('updated_on', 'datetime',
          update=request.now),
    Field('updated_by', 'reference auth_user',
          default=auth.user_id,
          update=auth.user_id),
    Field('is_active', 'boolean', default=True)
]

# create all tables needed by auth if not custom tables
auth.define_tables(username=True, signature=False, migrate=True)
db.auth_user._format = '%(first_name)s %(last_name)s'
current.auth = auth

# create administrator account
if db(db.auth_user.id > 0).count() == 0:
    db.executesql("DELETE FROM SQLITE_SEQUENCE WHERE NAME = 'auth_user]';")
    db.commit()
    auth.get_or_create_user(dict(first_name='ADMINISTRATOR',
                                 username='ADMINISTRATOR',
                                 created_by=None,
                                 updated_by=None))
    db.commit()

# update to bootstrap3 styles
import bootstrap3 as bs3

auth.settings.formstyle = bs3.form('horizontal')

# configure auth policy
auth.settings.hmac_key = settings.security_key\

auth.settings.actions_disabled=['profile', 'logout', 'register',
                                'change_password','request_reset_password']

# after defining tables, uncomment below to enable auditing
auth.enable_record_versioning(db)

signature = db.Table(db, 'signature',
                     Field('created_on', 'datetime',
                           default=request.now),
                     Field('created_by', db.auth_user,
                           default=auth.user_id),
                     Field('updated_on', 'datetime',
                           update=request.now),
                     Field('updated_by', db.auth_user,
                           default=auth.user_id,
                           update=auth.user_id),
                     Field('is_active', 'boolean', default=True))
