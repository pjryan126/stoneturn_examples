# -*- coding: utf-8 -*-
db.define_table('status_note',
                Field('legacy_id',
                      'integer'),
                Field('case_id',
                      'reference case_file'),
                Field('phase', 'string',
                      requires=IS_IN_SET(PHASES),
                      label='Phase'),
                Field('comment', 'text',
                      label='Comment'),
                Field('next_steps',
                      'text',
                      label='Next Steps'),
                Field('reported_by',
                      'reference auth_user',
                      label='Reported By'),
                Field('status_date', 'datetime',
                      label='Reported On',
                      default=request.now),
                signature,
                migrate=settings.migrate)

db.status_note.status_date.represent = lambda d: d.strftime('%b %d %Y') if d else 'Not Provided'


@db.status_note.add_method.most_recent_note
def most_recent_note(self, case_id):
    query = (self.case_id==case_id)
    return db(query).select(orderby=~self.status_date).first() or Storage()

