"""
from gluon.contrib.populate import populate

populate(db.auth_user, 10)
populate(db.segment, 5)
populate(db.region, 5)
populate(db.country, 5)
populate(db.state_province, 50)
populate(db.fraud_type, 5)
populate(db.case_file, 25)
populate(db.status_note, 150)
populate(db.tsheet, 150)
"""
