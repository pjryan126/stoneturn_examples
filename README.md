# StoneTurn Code Examples

## concurimport (Custom Python ETL package)

### Synopsis

This is a python package to handle the ETL process for loading weekly .csv data extracts from the Concur expense-approval system
to a SQL Server environment. To date, the package has loaded ~ 30 million records from 565 .csv files to corresponding tables into a SQL Server database.

Table Name			|	Record Count
--------------------|-----------------
ConcurBatch			|	565
ConcurApprover		|	7,598,356
ConcurAttendee		|	3,400,700
ConcurTransaction	|	6,449,559
ConcurUnpaid		|	12,390,948

### Motivation

A Python ETL process (in place of SSIS) allows for data loads to be called easily from a web interface. There is a 
companion prototype web application that allows investigators to load Concur data with the selection of a file and a click of a button. 

### Installation and Execution

1. Create and 'cd' into an empty directory.
2. Copy concurimport package into directory.
3. Add Concur Files for import into SourceFiles folder
4. Execute the following commands:

```
$ virtualenv --no-site-packages venv
$ source venv/bin/activate # Linux/BSD only. For windows use venv\scripts\activate
$ pip install -r requirements.txt # keep an eye out for pyodbc. Windows requires installation from a .exe file
$ cd concurimport
$ python run.py
```

### NOTES ##

This script is intended solely as an example of using Python code to process large quantities of data. It will not
execute without valid source files. 

## CaseAppPrototype

### Synopsis

This is a sanitized version of a case tracking web application that I built for AIG's internal fraud investigations group. The application scrapes data from the original applications, which is still in use, presents it in a more accessible format, and allows investigators to add status updates on their individual cases. 

### Motivation

AIG's investigations group utilizes several aging web applications whose backend database management systems make many tasks all but impossible for investigators to perform. In particular, investigators have had no way to communicate status updates to their supervisors, and no way to provide historical information related to their investigations. This application was designed to leave the current case tracking application untouched while extending its functionality and paving the way for a full application upgrade.

### Installation and Execution

1. Open the CaseAppPrototype folder
2. Double-click the "start" file
3. In a Firefox or Chrome browser (IE 10+ will also work), copy and paste 127.0.0.1:8080 into the address bar and press ENTER.

## SQL

### Synopsis

Contained in this folder are two samples of SQL scripts developed to cleanse and report on data that was previously incomprehensible to end users.

### Motivation

#### ReportInvestigations.sql
The preexisting technology group responsible for developing intranet  
applications for the company's audit division appears to have had no 
experience developing and maintaining SQL databases. The investigations 
group must be able to provide historically accurate reports at a moment's,  
notice, but the technology group has never implemented a record versioning
solution. Additionally, the data validation rules have changed on 
multiple columns numerous times throughout the years, always without 
documentation. The data analytics team is not authorized to make schema
changes to the technology group's databases. 

This table-valued function provides a relation of all investigations data
as it existed on the date requested during execution. The data is reliable
from January 1, 2013 onward (the date at which the analytics team assumed
ownership of the investigations group's historical reporting requirements).

In its first iteration, the investigations team maintained a simple
CREATE_TABLE script, along with some helper scripts, that would be run 
manually whenever an updated report of investigations was requested. This
allowed for the team to create historical snapshot tables whenever it ran
the script, but it was a manually intensive process (the scripts were not
production-quality), and it was cluttering the database with little-used
historical tables.
--
This function relies on three tables to record data transformations
("lenses") and the reports to which each such lens refers. By storing 
these lenses in tables, rather than in a CREATE_TABLE script, the data 
is constantly up-to-date and can be queried to provide historical values
as of any given date between January 1, 2013, and today.  

#### UpdateCaseData.sql

This script is called and executed from a Python script running as a nightly cron job.

The purpose of the script is to pull investigations data from the technology group's
databases, cleanse it for use and review using a table-valued function, and 
load it into analytics tables that follow basic relational principles and RDBMS best practices. 
The analytics tables are used in connection with a web application that allows investigators 
to view and create reports online.  
