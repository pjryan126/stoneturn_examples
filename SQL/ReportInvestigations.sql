USE [CaseTrak_Dev];
GO
/****** Object:  UserDefinedFunction [WRK].[CTS_Investigations]    Script Date: 12/04/2015 14:21:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [WRK].[CTS_Investigations](@rptDate datetime)
RETURNS @rtnTable TABLE (
      CaseId					          int
    , CaseNumber                nvarchar(8)
    , Name                      nvarchar(64)
    , CaseTypePrimary           nvarchar(64)
    , CaseTypeSecondary         nvarchar(64)
    , CaseTypeTertiary          nvarchar(64)
    , IsActive	                bit
    , CaseDescription           nvarchar(max)
    , CaseConclusion            nvarchar(max)
    , StateProvince             nvarchar(2)
    , Country                   nvarchar(128)
    , Region                    nvarchar(64)
    , BusinessSegment           nvarchar(128)
    , KeySubject                nvarchar(128)
    , LineOfBusiness            nvarchar(100)
    , CaseYear                  int
    , ExposureAmount            money
    , RecoveryAmount            money  
    , DirectorId				nvarchar(64) 
    , Director                  nvarchar(128)
    , SeniorManagerId			nvarchar(64)
    , SeniorManager             nvarchar(128)
    , CaseLeadId				nvarchar(64)
    , CaseLead                  nvarchar(128)
    , ResponsibleExecutive		nvarchar(128)
    , ConfirmedLoss             money
    , PendingRecovery           money
    , IsFraud                   bit
    , IsControlIssue            bit
    , IsPrivileged				bit
    , StartedOn                 smalldatetime
    , ClosedOn                  smalldatetime
    , ClosedBy                  nvarchar(32)
)

AS
BEGIN

-- Description -----------------------------------------------------------------
--
-- The preexisting technology group responsible for developing intranet  
-- applications for the company's audit division appears to have had no 
-- experience developing and maintaining SQL databases. The investigations 
-- group must be able to provide historically accurate reports at a moment's,  
-- notice, but the technology group has never implemented a record versioning
-- solution. Additionally, the data validation rules have changed on 
-- multiple columns numerous times throughout the years, always without 
-- documentation. The data analytics team is not authorized to make schema
-- changes to the technology group's databases. 
-- 
-- This table-valued function provides a relation of all investigations data
-- as it existed on the date requested during execution. The data is reliable
-- from January 1, 2013 onward (the date at which the analytics team assumed
-- ownership of the investigations group's historical reporting requirements).
-- 
-- In its first iteration, the investigations team maintained a simple
-- CREATE_TABLE script, along with some helper scripts, that would be run 
-- manually whenever an updated report of investigations was requested. This
-- allowed for the team to create historical snapshot tables whenever it ran
-- the script, but it was a manually intensive process (the scripts were not
-- production-quality), and it was cluttering the database with little-used
-- historical tables.
--
-- This function relies on three tables to record data transformations
-- ("lenses") and the reports to which each such lens refers. By storing 
-- these lenses in tables, rather than in a CREATE_TABLE script, the data 
-- is constantly up-to-date and can be queried to provide historical values
-- as of any given date between January 1, 2013, and today.
--
-- -----------------------------------------------------------------------------

-- Revision History ------------------------------------------------------------
--
-- AIG Email              Date        Description
-- patrick.ryan@aig.com   2015-06-15  First build of the script
-- patrick.ryan@aig.com   2015-07-23  Modify field names
-- patrick.ryan@aig.com   2015-09-03  Optimize query with left outer joins in
--                                    place of original cross-queries
--
-- -----------------------------------------------------------------------------

-- Relationships ---------------------------------------------------------------
--
-- Source Table(s):         [AnalyticsInvestigation].[RPT].[Report]
--                          [AnalyticsInvestigation].[RPT].[Lens]
--                          [AnalyticsInvestigation].[RPT].[ReportLens]
--                          [AnalyticsInvestigation].[CTS].[CaseTaxonomy]
--                          [ATS].[dbo].[EMPLOYEE]
--                          [CTS].[dbo].[TRK_CASES]
--                          [AnalyticsInvestigation].[CTS].[ReportCaseCountry]
--                          
-- -----------------------------------------------------------------------------

-- Execution -------------------------------------------------------------------
--
-- Use any date between January 1, 2013, and today to view the data 
-- on that date:
--
-- SELECT * FROM [WRK].[CTS_Investigations](GETDATE())
--
-- To add new lenses for the data, update the [RPT].[Report], [RPT].[Lens], and
-- [RPT].[RptLens] fields as needed. The EffectiveDate field in the 
-- [Lens].[ReportLens] table dictates whether the lens should be applied as of 
-- the report date provided when executing this function.
--
-- -----------------------------------------------------------------------------

-- Next Steps ------------------------------------------------------------------
--
-- Script is in production. Consider moving 'CASE WHEN' logic to report lens 
-- tables to increase performance.
-- 
-- -----------------------------------------------------------------------------

-- Get a relation of all report lenses created as of the report date. 
-- The pivot clause gets the most recent report lens data as of the date 
-- provided when executing the script.

WITH LENS AS
(
	SELECT *
	FROM
	(
		SELECT 
			  SelectorColumn
			, SelectorValue
			, LensColumn
			, LensValue
			, EffectiveDate
		FROM 
			(
				SELECT 
					  RL.ReportLensId										ReportLensId
					, RL.SelectorColumn										SelectorColumn
					, RL.SelectorValue										SelectorValue
					, L.LensColumn											LensColumn
					, L.LensValue											LensValue
					, RL.EffectiveDate										EffectiveDate
					, RANK() OVER (PARTITION BY 
									  L.LensColumn
									, RL.SelectorValue 
								   ORDER BY 
								      RL.EffectiveDate DESC
								    , RL.ReportLensId DESC)					LensRank
				FROM 
					  [AnalyticsInvestigation].[RPT].[Report] R
					, [AnalyticsInvestigation].[RPT].[Lens] L
					, [AnalyticsInvestigation].[RPT].[ReportLens] RL
				WHERE R.ReportId = RL.ReportId
				  AND RL.LensId = L.LensId
				  AND R.ReportId = 1
				  AND RL.EffectiveDate <= @rptDate
			) A
		WHERE LensRank = 1
		) SRC
	PIVOT
	(
	  MAX(LensValue)
	  FOR LensColumn in (
		  BusinessSegment
		, CaseMatter
		, CaseStatus
		, CaseType
		, Country
		, KeySubject
		, LineOfBusiness
		, ProjectType
		, SeniorManager
		, StartedOn
		)
	) P
),
-- Get employee data for user-related fields.
E AS
(
    SELECT
         UPPER(LoginID) AS LoginId
        ,EmployeeName AS EmployeeName
    FROM [ATS].[dbo].[EMPLOYEE]
),
-- Get case/investigation data.
C AS 
(
    SELECT 
		  CAST(C.uid AS int)								CaseId
        , C.[PROJECT]                                       CaseNumber
        , C.[DESCRIP]                                       Name
        , C.[Long_Desc]                                     CaseDescription
        , C.[Conclusion]                                    CaseConclusion
        , C.[State]                                         StateProvince
        , C.[LossAmt]                                       ExposureAmount
        , C.[RecovAmt]                                      RecoveryAmount
        , C.[Company]                                       Company
        , C.[CaseYear]                                      CaseYear
        , C.[SeniorManager]									Director
        , C.[Manager]                                       SeniorManager
        , C.[Lead]                                          CaseLead
        , C.[RspnsblExecutive]								ResponsibleExecutive
        , C.[ConfirmedLossAmt]                              ConfirmedLoss
        , C.[PendingRecoveredAmt]                           PendingRecovery
        , CASE 
			WHEN C.[Fraud] = 'Y' THEN 1
			ELSE 0
		  END		                                        IsFraud
        , CASE 
			WHEN C.[ControlIssue] = 'Y' THEN 1              
			ELSE 0
		  END												IsControlIssue
		, CASE
			WHEN C.[Privileged] = 'Y' THEN 1
			ELSE 0
		  END												IsPrivileged
        , C.[case_closed_dt]                                ClosedOn
        , C.[Close_By]                                      ClosedBy
        , CASE
            WHEN CL.Country IS NOT NULL 
              THEN CL.Country
            WHEN C.[Country] LIKE '%*United States%' 
              THEN 'United States of America'
            WHEN C.[Country] LIKE 'UAE' 
              THEN 'United Arab Emirates'
            WHEN C.[Country] LIKE '%Korea%' 
              THEN 'Korea, Republic of'
            WHEN C.[Country] LIKE 'Phillippines' 
              THEN 'Philippines'
            WHEN C.[Country] LIKE '%Hong Kong%' 
              THEN 'Hong Kong'
            WHEN C.[Country] LIKE 'Holland' 
              THEN 'Netherlands'
            WHEN C.[Country] LIKE '%Taiwan%' 
              THEN 'Taiwan, Province of China'
            WHEN C.[Country] LIKE '%Vietnam%'
              THEN 'Viet Nam'
            WHEN C.[Country] LIKE '%Russia%'
              THEN 'RUSSIAN FEDERATION'
            ELSE CONVERT(varchar(max), C.[Country])
          END												Country
        , CASE
            WHEN GL_CS.CaseStatus IS NOT NULL  
              THEN GL_CS.CaseStatus
            ELSE CONVERT(varchar(max), C.[STATUS])
          END                                               CaseStatus  
        , CASE
            WHEN CL.BusinessSegment IS NOT NULL
              AND CL.EffectiveDate >= 
                ISNULL(GL_BS.EffectiveDate, 
                       '1900-01-01')
              THEN CL.BusinessSegment
            WHEN GL_BS.BusinessSegment IS NOT NULL 
              THEN GL_BS.BusinessSegment
            ELSE CONVERT(varchar(max), 
              LTRIM(RTRIM(C.[Bsns_Sgmnt])))
          END                                               BusinessSegment
        , CASE
            WHEN CL.KeySubject IS NOT NULL 
              THEN CL.KeySubject
            ELSE CONVERT(varchar(max), C.[KeySubject])
          END                                               KeySubject
        , CASE
            WHEN CL.LineOfBusiness IS NOT NULL 
              THEN CL.LineOfBusiness
            ELSE CONVERT(varchar(max), 
                         C.[LOBDescription])
          END                                               LineOfBusiness  
        , CASE
            WHEN CL.StartedOn IS NOT NULL 
              THEN CL.StartedOn
            ELSE ISNULL(C.START_DT, C.ENTER_DT)
          END                                               StartedOn
        , CASE
            WHEN CL.ProjectType IS NOT NULL
              AND CL.EffectiveDate >= 
                ISNULL(GL_PT.EffectiveDate, 
                       '1900-01-01')
              THEN CL.ProjectType
            WHEN GL_PT.ProjectType IS NOT NULL 
              THEN GL_PT.ProjectType
            ELSE CONVERT(varchar(max), 
                         C.[PROJECTTYPE])
          END                                               ProjectType
        , CASE
            WHEN CL.CaseType IS NOT NULL 
              THEN CL.CaseType
            WHEN GL_T.CaseType IS NOT NULL 
              THEN GL_T.CaseType
            ELSE CONVERT(varchar(max), C.[type]) 
          END                                               CaseType         
    FROM [CTS].[dbo].[TRK_CASES] C
        LEFT OUTER JOIN LENS CL
          ON C.[Case_No] = CL.SelectorValue
        LEFT OUTER JOIN LENS GL_BS
          ON C.[Bsns_Sgmnt] = GL_BS.SelectorValue
          AND GL_BS.SelectorColumn = 'Bsns_Sgmnt'
        LEFT OUTER JOIN LENS GL_PT
          ON C.[projecttype] = GL_PT.SelectorValue
          AND GL_PT.ProjectType IS NOT NULL
        LEFT OUTER JOIN LENS GL_CS
          ON C.[projecttype] = GL_CS.SelectorValue
          AND GL_CS.CaseStatus IS NOT NULL
        LEFT OUTER JOIN LENS AS GL_T
          ON C.[type] = GL_T.SelectorValue
          AND GL_T.SelectorColumn = 'type'
)
INSERT INTO @rtnTable
SELECT
	  C.CaseId					                            CaseId 
    , C.CaseNumber                                          CaseNumber
    , C.Name                                                Name
    , T.CaseTypeCat2                                        CaseTypePrimary
    , T.CaseTypeCat1                                        CaseTypeSecondary
    , T.CaseType                                            CaseTypeTertiary
    , CASE WHEN C.CaseStatus = 'A'
		      THEN 1
		    ELSE 0
	    END AS					                            IsActive
    , C.CaseDescription                                     CaseDescription
    , C.CaseConclusion                                      CaseConclusion
    , C.StateProvince                                       StateProvince
    , C.Country                                             Country
    , ISNULL(R.Reporting, 
             'Not Provided')                                Region
    , C.BusinessSegment                                     BusinessSegment
    , C.KeySubject                                          KeySubject
    , C.LineOfBusiness                                      LineOfBusiness
    , C.CaseYear                                            CaseYear
    , C.ExposureAmount                                      ExposureAmount
    , C.RecoveryAmount                                      RecoveryAmount  
    , C.Director				                            DirectorId
    , D.EmployeeName                                        Director
    , C.SeniorManager			                            SeniorManagerId
    , S.EmployeeName			                            SeniorManager
    , C.CaseLead				                            CaseLeadId
    , M.EmployeeName                                        CaseLead
    , C.ResponsibleExecutive	                            ResponsibleExecutive
    , C.ConfirmedLoss                                       ConfirmedLoss
    , C.PendingRecovery                                     PendingRecovery
    , C.IsFraud                                             IsFraud
    , C.IsControlIssue                                      IsControlIssue
    , C.IsPrivileged			                            IsPrivileged
    , C.StartedOn                                           StartedOn
    , C.ClosedOn                                            ClosedOn
    , C.ClosedBy                                            ClosedBy
FROM C
LEFT OUTER JOIN [AnalyticsInvestigation].[CTS].[CaseTaxonomy] T
  ON C.CaseType = T.[CaseType] 
LEFT OUTER JOIN [AnalyticsInvestigation].[CTS].[ReportCaseCountry] R
  ON C.Country = R.Country
LEFT OUTER JOIN E D
  ON C.Director = D.LoginID
LEFT OUTER JOIN E S
  ON C.SeniorManager = S.LoginID
LEFT OUTER JOIN E M
  ON C.CaseLead = M.LoginID
WHERE C.ProjectType = 'Investigations'

RETURN;
END;

