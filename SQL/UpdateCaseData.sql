-- This script is called and executed from a Python script running as a nightly cron job.
--
-- The purpose of the script is to pull investigations data from the technology group's
-- databases, cleanse it for use and review using a table-valued function, and 
-- load it into analytics tables that follow basic relational principles and RDBMS best practices. 
-- The analytics tables are used in connection with a web application that allows investigators 
-- to view and create reports online.  

SET ANSI_NULLS ON;
SET ANSI_WARNINGS ON;

INSERT INTO [App].[Case] (
    [LegacyId]
  , [CreatedOn]
  , [CreatedBy]
  , [UpdatedOn]
  , [UpdatedBy]
  , [IsActive]
)
SELECT
    I.[CaseId]    [LegacyId]
  , GETDATE()     [CreatedOn]
  , 1             [CreatedBy] -- Administrator user_id
  , GETDATE()     [UpdatedOn]
  , 1             [UpdatedBy] -- Administrator user_id
  , I.IsActive    [IsActive]
FROM [WRK].[CTS_Investigations]() I
LEFT OUTER JOIN [dbo].[auth_user] D
  ON I.[DirectorId] = D.[username]
LEFT OUTER JOIN [dbo].[auth_user] S
  ON I.[SeniorManagerId] = S.[username]
LEFT OUTER JOIN [dbo].[auth_user] L
  ON I.[CaseLeadId] = L.[username]
LEFT OUTER JOIN [dbo].[auth_user] C
  ON I.[CaseLeadId] = C.[username]
LEFT OUTER JOIN [dbo].[auth_user] E
  ON I.[CaseLeadId] = E.[username]
WHERE YEAR(I.StartedOn) >= 2013
  AND I.[CaseId] NOT IN (SELECT [LegacyId] FROM [App].[Case]);

UPDATE I
SET
    I.[CaseNumber] = O.[CaseNumber]
  , I.[Name] = O.[Name]
  , I.[Description] = O.[Description]
  , I.[Conclusion] = O.[Conclusion]
  , I.[SegmentId] = O.[SegmentId]
  , I.[RegionId] = O.[RegionId]
  , I.[CountryId] = O.[CountryId]
  , I.[StateProvinceId] = O.[StateProvinceId]
  , I.[KeySubject] = O.[KeySubject]
  , I.[DirectorId] = O.[DirectorId]
  , I.[SeniorManagerId] = O.[SeniorManagerId]
  , I.[LeadInvestigatorId] = O.[CaseLeadId]
  , I.[ResponsibleExecutive] = O.[ResponsibleExecutive]
  , I.[ExposureAmount] = O.[ExposureAmount]
  , I.[ConfirmedLoss] = O.[ConfirmedLoss]
  , I.[PendingRecovery] = O.[PendingRecovery]
  , I.[LastActivityOn] = O.[LastActivityOn]
  , I.[IsControlIssue] = O.[IsControlIssue]
  , I.[IsPrivileged] = O.[IsPrivileged]
  , I.[IsFraud] = O.[IsFraud]
  , I.[OpenedOn] = O.[OpenedOn]
  , I.[OpenedBy] = O.[OpenedBy]
  , I.[ClosedOn] = O.[ClosedOn]
  , I.[ClosedBy] = O.[ClosedBy]
  , I.[TotalHours] = O.[TotalHours]
  , I.[UpdatedOn] = O.[UpdatedOn]
  , I.[UpdatedBy] = O.[UpdatedBy]
  , I.[IsActive] = O.[IsActive]
FROM [App].[Case] I
INNER JOIN (
  SELECT
	  I.[CaseId]				  [LegacyId]
  , I.[CaseNumber]                [CaseNumber]
  , I.[Name]                      [Name]
  , I.[CaseDescription]           [Description]
  , I.[CaseConclusion]            [Conclusion]
  , B.[SegmentId]				  [SegmentId]
  , R.[RegionId]                  [RegionId]
  , C.[CountryId]				  [CountryId]
  , S.[StateProvinceId]           [StateProvinceId]
  , I.[KeySubject]                [KeySubject]
  , D.[id]                        [DirectorId]
  , M.[id]                        [SeniorManagerId]
  , L.[id]                        [CaseLeadId]
  , I.[ResponsibleExecutive]      [ResponsibleExecutive]
  , I.[ExposureAmount]            [ExposureAmount]
  , I.[ConfirmedLoss]             [ConfirmedLoss]
  , I.[PendingRecovery]           [PendingRecovery]
  , A.[LastActivityOn]            [LastActivityOn]
  , I.[IsControlIssue]            [IsControlIssue]
  , I.[IsFraud]                   [IsFraud]
  , I.[IsPrivileged]              [IsPrivileged]
  , I.[StartedOn]                 [OpenedOn]
  , E.[id]                        [OpenedBy]
  , I.[ClosedOn]                  [ClosedOn]
  , CL.[id]                       [ClosedBy]
  , T.[TotalHours]                [TotalHours]
  , GETDATE()                     [UpdatedOn]
  , 1                             [UpdatedBy]
  , I.IsActive                    [IsActive]
  FROM [WRK].[CTS_Investigations]() I
  LEFT OUTER JOIN [App].[StateProvince] S
    ON I.StateProvince = S.[Abbreviation]
  LEFT OUTER JOIN [App].[Segment] B
    ON I.BusinessSegment = B.Name
  LEFT OUTER JOIN [App].[Region] R
    ON I.[Region] = R.[Name]
  LEFT OUTER JOIN [App].[Country] C
    ON I.Country = C.Name
  LEFT OUTER JOIN [dbo].[auth_user] D
    ON I.[DirectorId] = D.[username]
  LEFT OUTER JOIN [dbo].[auth_user] M
    ON I.[SeniorManagerId] = M.[username]
  LEFT OUTER JOIN [dbo].[auth_user] L
    ON I.[CaseLeadId] = L.[username]
  LEFT OUTER JOIN (
    SELECT
        uid AS [CaseId]
      , MAX(lastupdateddt) AS [LastActivityOn]
    FROM [CTS].[dbo].[TRK_Activity_Notes]
    GROUP BY uid
    ) A
    ON I.[CaseId] = A.[CaseId]
  LEFT OUTER JOIN [dbo].[auth_user] CL
    ON I.[CaseLeadId] = CL.[username]
  LEFT OUTER JOIN [dbo].[auth_user] E
    ON I.[CaseLeadId] = E.[username]
  LEFT OUTER JOIN (
    SELECT
       T.[CaseNumber]
      ,SUM(T.[Hours]) AS TotalHours
    FROM [App].[TimeSheet] T
    GROUP BY T.[CaseNumber]
  )T
    ON I.[CaseNumber] = T.[CaseNumber]
  WHERE YEAR(I.StartedOn) >= 2013
) O
  ON I.[LegacyId] = O.[LegacyId];