#!/usr/bin/env python
from collections import OrderedDict
from sqlalchemy.orm.session import Session
from database import Base
from models import ConcurTransaction
import os
import unittest


class TestExcelImport(unittest.TestCase):

    def setUp(self):
        self.engine = create_engine('mssql+pyodbc://spotfireappid:sp0tf1re@10.90.242.133/Monitor')
        self.Session = Session(self.engine)

        Base.metadata.create_all(self.engine)

    def tearDown(self):
        Base.metadata.drop_all(self.engine)


class TestParentExcelImport(TestExcelImport):

    def setUp(self):
        super(TestExcelImport, self).setUp()
        self.fname = 'parent_test_data.xlsx'
        self.fpath = os.path.abspath('./test_files')

    def tearDown(self):
        super(TestExcelImport, self).tearDown()
        self.fname = None
        self.fpath = None

    def test_extract_xls(self):
        rows = ConcurTransaction._extract_xls(self.fname, self.fpath, start_row=2)
        self.assertEqual(len(rows), 1001)

    def test_transform_rows(self):
        rows = ConcurTransaction._extract_xls(self.fname, self.fpath, start_row=2)
        tr_rows = ConcurTransaction._transform_rows(rows)
        self.assertEqual(len(rows), len(tr_rows))

    def test_transform_field_data_string(self):
        val = ConcurTransaction._transform_field_data(1, 5000.34)[1]
        self.assertTrue(isinstance(val, str))









