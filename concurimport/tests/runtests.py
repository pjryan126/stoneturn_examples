#!/usr/bin/env python
import unittest
import os
import sys
sys.path.append(os.path.abspath('..'))
from TestEtlMixIn import TestParentExcelImport


def run():
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(TestParentExcelImport))
    unittest.TextTestRunner().run(test_suite)

if __name__ == '__main__':
    run()