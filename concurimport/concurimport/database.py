from sqlalchemy import (
    Column,
    create_engine,
    Integer,
)
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
)
from sqlalchemy.ext.declarative import declarative_base
import urllib


server = '10.90.242.133'
db_name = 'FraudMonitor_Dev'
dsn = urllib.quote_plus(''.join([
    'DRIVER={SQL SERVER};'
    'SERVER=%s;' % server,
    'DATABASE=%s;UID=DevAppId;PWD=P3ngu1n_5lies;port=1433;' % db_name]))

engine = create_engine('mssql+pyodbc:///?odbc_connect={0}'.format(dsn), echo=False)
Session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))


class BaseModel(object):

    @declared_attr
    def __tablename__(cls):
        return cls.__name__

    @declared_attr
    def __table_args__(cls):
        return {'schema': 'Concur'}

    @declared_attr
    def id(cls):
        return Column('%sId' % cls.__name__, Integer, primary_key=True)


Base = declarative_base(cls=BaseModel)
Base.query = Session.query_property()


def init_db():
    from models import *
    Session.remove()
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)