from database import Base
from sqlalchemy import (
    Boolean,
    Column,
    create_engine,
    DateTime,
    Float,
    ForeignKey,
    Integer,
    String,
)
from mixins import (
    ExcelEtlMixin,
    SignatureMixin,
)
from sqlalchemy.orm import relationship


class ConcurBatch(SignatureMixin, Base):
    """
    Batch excel file from which Concur records originate

    [schema].[table] = [Concur].[ConcurBatch]

    """

    __table_args__ = {"schema": "Concur"}

    filename = Column('FileName', String(100))
    url = Column('Url', String(255))


class ConcurTransaction(ExcelEtlMixin, Base):
    """A transaction listed in the Concur database

    [schema].[table] = [Concur].[ConcurTransaction]

    """

    __table_args__ = {"schema": "Concur"}

    policy = Column("Policy", String(130))
    os_cost_code = Column('OverseasCostCode', String(66))
    other_cost_code = Column('OtherCostCode', String(66))
    employee = Column('Employee', String(302))
    employee_id = Column('EmployeeId', String(258))
    report_id = Column('ReportId', String(66))
    report_name = Column('ReportName', String(82))
    created_on = Column('CreatedOn', DateTime)
    expense_type = Column('ExpenseType', String(130))
    transact_date = Column('TransactionDate', DateTime)
    payment_type = Column('PaymentType', String(130))
    city = Column('CityOrLocation', String(130))
    vendor = Column('Vendor', String(130))
    from_location = Column('FromLocation', String(202))
    to_location = Column('ToLocation', String(202))
    meal_type = Column('MealType', String(66))
    airline_fee_type = Column('AirlineFeeType', String(66))
    flight_class = Column('FlightClass', String(66))
    is_personal = Column('IsPersonal', Boolean)
    purpose = Column('Purpose', String(130))
    txn_type = Column('TransactionType', String(202))
    entry_key = Column('EntryKey', String(50))
    parent_entry_key = Column('ParentEntryKey', String(50))
    rpt_currency = Column('ReportingCurrency', String(8))
    rpt_approved_amt = Column('ReportingApprovedAmount',
                              Float(precision=10, asdecimal=True, decimal_return_scale=2),
                              nullable=True)
    reimb_currency = Column('ReimbursementCurrency', String(8))
    reimb_exp_amt = Column('ReimbursementExpenseAmount',
                           Float(precision=10, asdecimal=True, decimal_return_scale=2),
                           nullable=True)
    reimb_approved_amt = Column('ReimbursementApprovedAmount',
                            Float(precision=10, asdecimal=True, decimal_return_scale=2),
                            nullable=True)
    assoc_cc_transact_key = Column('AssociatedCreditCardTransactionKey', String(50))
    sent_for_payment = Column('SentForPayment', DateTime)
    batch_id = Column('ConcurBatchId', ForeignKey('Concur.ConcurBatch.ConcurBatchId'))
    batch = relationship('ConcurBatch')


class ConcurAttendee(ExcelEtlMixin, Base):

    __table_args__ = {"schema": "Concur"}

    policy = Column('Policy', String(130))
    other_cost_code = Column('OtherCostCode', String(66))
    os_cost_code = Column('OverseasCostCode', String(66))
    employee = Column('Employee', String(302))
    employee_id = Column('EmployeeId', String(258))
    rpt_id = Column('ReportId', String(66))
    rpt_name = Column('ReportName', String(82))
    created_date = Column('CreatedDate', DateTime)
    transact_date = Column('TransactionDate', DateTime)
    expense_type = Column('ExpenseType', String(130))
    expense_amt = Column('ExpenseAmount',
                         Float(precision=10, asdecimal=True, decimal_return_scale=2))
    appd_amt = Column('ApprovedAmount',
                      Float(precision=10, asdecimal=True, decimal_return_scale=2))
    rpt_appd_amt = Column('ReportingApprovedAmount',
                          Float(precision=10, asdecimal=True, decimal_return_scale=2))
    num_attendees = Column('NumberOfAttendees',
                           Integer)
    payment_type = Column('PaymentType', String(130))
    vendor = Column('Vendor', String(130))
    city = Column('CityOrLocation', String(130))
    attendee_name = Column('AttendeeName', String(402))
    title = Column('Title', String(66))
    company = Column('Company', String(302))
    attendee_type = Column('AttendeeType', String(82))
    is_personal = Column('IsPersonal', Boolean, default=False)
    entry_key = Column('EntryKey', String(50))
    transact_type = Column('TransactionType', String(202))
    purpose = Column('Purpose', String(130))
    batch_id = Column('BatchId', ForeignKey('Concur.ConcurBatch.ConcurBatchId'))
    batch = relationship('ConcurBatch')


class ConcurApprover(ExcelEtlMixin, Base):

    __table_args__ = {"schema": "Concur"}

    cbd_code = Column('CbdCode', String(66))
    intl_cost_center = Column('IntlCostCenter', String(66))
    step = Column('Step', String(258))
    assigned_employee = Column('EmployeeAssignedToStep', String(302))
    employee = Column('Employee', String(302))
    employee_id = Column('EmployeeId', String(258))
    rpt_name = Column('ReportName', String(82))
    rpt_id = Column('ReportId', String(66))
    rpt_amt = Column('TotalReportAmount',
                     Float(precision=10, asdecimal=True, decimal_return_scale=2))
    sent_for_payment = Column('SentForPayment', DateTime)
    payment_status = Column('PaymentStatus', String(130))
    expense_type = Column('ExpenseType', String(130))
    transact_date = Column('TransactionDate', DateTime)
    vendor = Column('Vendor', String(130))
    purpose = Column('Purpose', String(130))
    city = Column('CityOrLocation', String(130))
    payment_type = Column('PaymentType', String(130))
    is_personal = Column('IsPersonal', Boolean, default=False)
    appd_amt = Column('ApprovedAmount',
                      Float(precision=10, asdecimal=True, decimal_return_scale=2))
    entry_key = Column('EntryKey', String(50))
    batch_id = Column('BatchId', ForeignKey('Concur.ConcurBatch.ConcurBatchId'))
    batch = relationship('ConcurBatch')


class ConcurUnpaid(ExcelEtlMixin, Base):

    __table_args__ = {"schema": "Concur"}

    emp_org_code = Column('EmployeeOrgCode', String(66))
    employee = Column('Employee', String(512))
    employee_id = Column('EmployeeId', String(258))
    assignment_status = Column('AssignmentStatus', String(24))
    default_expense_type = Column('DefaultExpenseType', String(130))
    reimb_currency = Column('ReimbursementCurrency', String(8))
    posted_amt = Column('PostedAmount',
                        Float(precision=10, asdecimal=True, decimal_return_scale=2))
    transact_amt = Column('TransactionAmount',
                          Float(precision=10, asdecimal=True, decimal_return_scale=2))
    transact_currency = Column('TransactionCurrency', String(8))
    merchant = Column('Merchant', String(258))
    transact_date = Column('TransactionDate', DateTime)
    cc_transact_key = Column('CreditCardTransactionKey', Integer)
    batch_id = Column('ConcurBatchId', ForeignKey('Concur.ConcurBatch.ConcurBatchId'))
    batch = relationship('ConcurBatch')