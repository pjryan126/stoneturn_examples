from database import Session, init_db
from datetime import datetime
from models import *
import os
from sqlalchemy import update
import re
import shutil
import time
from xlrd import (
    open_workbook,
    xldate_as_tuple,
)

session = Session()


def main():
    o_start = time.time()

    try:
        objects = {
            re.compile('Parent'): ['Parent', ConcurTransaction],
            re.compile('Child'): ['Child', ConcurTransaction],
            re.compile('Attendee'): ['Attendee', ConcurAttendee],
            re.compile('Approval'): ['Approval', ConcurApprover],
            re.compile('Unpaid'): ['Unpaid', ConcurUnpaid]
        }

        fpath = os.path.abspath('../SourceFiles')
        num_files = len(os.listdir(fpath)) - 1
        for f in os.listdir(fpath):
            if os.path.isfile(os.path.join(fpath, f)):
                start = time.time()
                wb = open_workbook(os.path.join(fpath, f))

                ws = wb.sheet_by_index(wb.nsheets - 1)
                try:
                    run_date = xldate_as_tuple(ws.cell_value(ws.nrows - 1, 0),
                                           wb.datemode)
                    run_date = datetime(*run_date)
                    run_date = datetime.strftime(run_date, '%Y-%m-%d')
                except:
                    print 'Could not find date for %s' % f
                    continue
                for o in objects:
                    if o.search(f):
                        obj_type = objects[o][0]
                        obj = objects[o][1]
                        batch = ConcurBatch()
                        fname, ext = os.path.splitext(f)
                        batch.filename = ''.join([obj_type, 'Entries_', run_date, ext])
                        batch.url = os.path.abspath(os.path.join(fpath, 'ProcessedFiles', batch.filename))
                        batch.is_active = False
                        loaded = session.query(ConcurBatch).\
                            filter_by(filename=batch.filename).first()
                        if loaded is None:
                            shutil.copy(os.path.abspath(os.path.join(fpath, f)), batch.url)
                            session.add(batch)
                            session.flush()
                            print "########## Processing data from %s ##########" % batch.filename
                            if isinstance(obj, ConcurUnpaid):
                                u_batches = session.query(ConcurBatch).filter_by(ConcurBatch.filename.like('%ConcurUnpaid%'))
                                u_batches.update(is_active=False)
                                session.add(u_batches)
                                session.flush()
                            obj.import_excel(session, batch)
                            batch.is_active = True
                            session.commit()
                            os.remove(os.path.abspath(os.path.join(fpath, f)))
                        else:
                            print 'Batch file already loaded (ConcurBatch Id: %s)' % loaded.id
                end = time.time()
                print 'File Load Time: %s' % (end - start)
        session.close()
        print "##########################################################################"
        o_time = time.time() - o_start
        print "Number of Files: %s" % num_files
        print "Average Processing Time: %s" % (o_time / num_files)
        print "Overall Processing Time: %s" % o_time
    except:
        session.rollback()
        session.close()
        raise
    return

if __name__ == '__main__':
    main()


