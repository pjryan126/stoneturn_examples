#!/usr/bin/env python
from database import Base, db_name, engine
import csv
from datetime import datetime
import os
from progressbar import ProgressBar, Percentage, Bar
from sqlalchemy import (
    Boolean,
    Column,
    Date,
    DateTime,
    Float,
    ForeignKey,
    Integer,
)
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import MapperExtension
import string
from xlrd import (
    open_workbook,
    xldate_as_tuple,
)


class SignatureExtension(MapperExtension):
    """ Mapper extension to populate signature columns
    on all models derived from SignatureMixin
    class below.

    """

    def before_insert(self, mapper, connection, instance):
        instance.created_on = datetime.utcnow()
        instance.modified_on = datetime.utcnow()

    def before_update(self, mapper, connection, instance):
        instance.created_on = instance.created_on
        instance.modified_on = datetime.utcnow()


class SignatureMixin(Base):
    """
    A mixin class to add signature columns to a table.

    Example:
    >>> class Subject(SignatureMixin, Base)
    ...     subject_id = Column(Integer, primary_key=True
    ...     desc = Column(String(100)
    >>> subject = Subject()
    >>> subject.created_on is not None
    True

    """

    __abstract__ = True
    __mapper_args__ = {'extension': SignatureExtension()}

    @declared_attr
    def created_on(cls):
        return Column('CreatedOn', DateTime, default=datetime.now())

    @declared_attr
    def modified_on(cls):
        return Column('ModifiedOn', DateTime, default=datetime.now(), onupdate=datetime.now())


    @declared_attr
    def is_active(cls):
        return Column('IsActive', Boolean, default=True)

class ExcelEtlMixin(object):

    @classmethod
    def import_excel(cls, session, batch):
        wb = open_workbook(batch.url)
        fpath = os.path.join(os.path.split(os.path.dirname(batch.url))[0],
                             'ProcessedFiles')
        fname = os.path.splitext(batch.filename)[0] + '.txt'
        full_path = os.path.join(fpath, fname)
        f = open(full_path, 'wb')
        writer = csv.writer(f,
                            delimiter='|',
                            quoting=csv.QUOTE_NONE,
                            quotechar='')
        for n in range(wb.nsheets):
            ws = wb.sheet_by_index(n)
            data = cls._extract_data(ws)
            rows = cls._transform_data(wb, data, batch.id)
            if n == wb.nsheets - 1:
                rows.pop(-1)
            for i, row in enumerate(rows):
                writer.writerow(row)
        f.close()
        print 'Loading to SQL...'
        sql = """
        BULK INSERT [%s].[%s].[%s]
        FROM '%s' WITH (
            FIELDTERMINATOR='|',
            ROWTERMINATOR='\\n'
            );
        """ % (db_name,
               cls.__table_args__['schema'],
               cls.__tablename__,
               full_path)
        session.execute(sql)
        session.flush()
        os.remove(full_path)
        return

    @classmethod
    def _extract_data(cls, sheet):

        print 'extracting data...'

        rows = []
        num_rows = sheet.nrows
        num_cols = sheet.ncols

        pbar = ProgressBar(widgets=[Percentage(), Bar()], maxval=100).start()
        increment = (num_rows / 100) + 1

        for i, r in enumerate(range(2, num_rows)):
            row = []
            for c in range(0, num_cols):
                row.append(sheet.cell_value(r,c))
            pbar.update(i/increment)
            rows.append(row)
        pbar.finish()

        return rows


    @classmethod
    def _transform_data(cls, wb, data, batch_id):

        print "\tTransforming data..."
        # prepare progress bar
        pbar = ProgressBar(widgets=[Percentage(), Bar()], maxval=100).start()
        increment = (len(data) / 100) + 1

        # create list for transformed row dictionaries
        rows = []

        for i, d in enumerate(data):
            # create a dictionary for transformed row data
            row = []
            for c, val in enumerate(d):
                key, value = cls._transform_field_data(wb, c, val)
                row.append(value)
            # Add blank values for batch_id and id fields
            row.extend([batch_id, ''])
            # Append row to rows list
            rows.append(row)
            pbar.update(i / increment)

        pbar.finish()

        return rows

    @classmethod
    def _get_datetime(cls, wb, value):
        if value == '':
            return ''
        else:
            v = datetime(*xldate_as_tuple(value, wb.datemode))
            return datetime.strftime(v, '%Y/%m/%d %H:%M:%S')

    @classmethod
    def _get_boolean(cls, value):
        if value in ('Y', 'T'):
            return 1
        elif value in ('N', 'F'):
            return 0
        else:
            return ''

    @classmethod
    def _get_integer(cls, value):
        if value == '':
            return ''
        else:
            return int(value)

    @classmethod
    def _get_decimal(cls, value):
        if value == '':
            return ''
        else:
            return float(value)

    @classmethod
    def _get_string(cls, value):
        if value == '':
            val = ''
        elif isinstance(value, (int, float)):
            val = str(int(value)).strip()
        else:
            value = value.strip().replace('\n', '').replace('|', ' ')
            val = filter(lambda x: x in string.printable, value)
        return val

    @classmethod
    def _transform_field_data(cls, wb, c, val):

        columns = [col for col in cls.__table__.columns]

        if isinstance(columns[c].type, (DateTime)):
            value = cls._get_datetime(wb, val)
        elif isinstance(columns[c].type, Boolean):
            value = cls._get_boolean(val)
        elif isinstance(columns[c].type, Integer):
            value = cls._get_integer(val)
        elif isinstance(columns[c].type, (Float)):
            value = cls._get_decimal(val)
        else:
            value = cls._get_string(val)
        return columns[c].name, value

    